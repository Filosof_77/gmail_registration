package com.gmail.datadriver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * Created with Intellij IDEA
 * User: filosof_77
 * Date: 15.01.16.
 * Time: 18:55
 */
public class GenerateDate {
    public static final String LETTERS = "ABCDEFGHIJKLMNOPQRSTUYWXYZ";
    private static final String DIGITS = "0123456789";
    private static final String STRING_DELIMITER = ",";
    private static final String SURNAME = "data/surname.txt";
    private static final String MALE_NAME = "data/malename.txt";


    public static String getString(String characters, int length) {
        char[] text = new char[length];
        for (int i = 0; i < length; i++) {
            text[i] = characters.charAt(new Random().nextInt(characters.length()));
        }
        return new String(text);
    }

    public static String getSurname() {
        return getWordFromFile(SURNAME);
    }

    /**
     * @return Random Male Name from list (file "MALENAME.txt")
     */
    public static String getMaleName() {
        return getWordFromFile(MALE_NAME);
    }


    public static String getWordFromFile(String fileName) {
        String stringFromFile = readFileToString(fileName);
        String[] FemaleNamesList = stringFromFile != null ? stringFromFile.split(STRING_DELIMITER) : new String[0];
        return FemaleNamesList[produceNumber(FemaleNamesList)];
    }

    private static int produceNumber(String[] list) {
        return new Random().nextInt(list.length);
    }

    private static String readFileToString(String filePath) {
        try (InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(filePath)) {
            StringBuilder fileData = new StringBuilder(1000);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));

            char[] buf = new char[1024];
            int numRead;
            while ((numRead = reader.read(buf)) != -1) {
                String readData = String.valueOf(buf, 0, numRead);
                fileData.append(readData);
                buf = new char[1024];
            }
            reader.close();
            return fileData.toString();
        } catch (IOException io) {
            io.printStackTrace();
            return null;
        }
    }

    public static String getRandomString(int length) {
        String string = DIGITS + LETTERS.toUpperCase() + DIGITS + LETTERS.toLowerCase();
        return getString(string, length);
    }
}
